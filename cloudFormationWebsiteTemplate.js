module.exports = {
  AWSTemplateFormatVersion: '2010-09-09',
  Parameters: {
    hostedName: {
      Type: 'String',
      Description: 'Base path to add to DNS Name.'
    },
    hostedZoneId: {
      Type: 'String',
      Description: 'The hosted zone the service is running in by ID.'
    },
    deploymentVersion: {
      Type: 'String',
      Description: 'The dynamic production deployment version path used in S3'
    },
    zoneIdForServiceDomain: {
      Type: 'String',
      Default: 'Z2FDTNDATAQYW2',
      Description: 'The zoneId for the CloudFront Hosted Zone in Route 53, if using Cloudfront: https://docs.aws.amazon.com/AWSCloudFormation/latest/UserGuide/aws-properties-route53-aliastarget.html#cfn-route53-aliastarget-hostedzoneid'
    }
  },

  Resources: {
    S3Bucket: {
      Type: 'AWS::S3::Bucket',
      Properties: {
        BucketName: { Ref: 'hostedName' },
        Tags: [
          {
            Key: 'ServiceUI',
            Value: { Ref: 'hostedName' }
          }
        ],
        LifecycleConfiguration: {
          Rules: [{
            Id: 'delete-incomplete-mpu-7days',
            Prefix: '',
            AbortIncompleteMultipartUpload: {
              DaysAfterInitiation: 7
            },
            Status: 'Enabled'
          }]
        }
      }
    },

    CloudFrontOriginAccessIdentity: {
      Type: 'AWS::CloudFront::CloudFrontOriginAccessIdentity',
      Properties: {
        CloudFrontOriginAccessIdentityConfig: {
          Comment: { Ref: 'hostedName' }
        }
      }
    },

    S3BucketPolicy: {
      Type: 'AWS::S3::BucketPolicy',
      Properties: {
        Bucket: { Ref: 'hostedName' },
        PolicyDocument: {
          Version: '2012-10-17',
          Statement: [
            {
              Sid: 'Grant a CloudFront Origin Identity access to support private content',
              Effect: 'Allow',
              Principal: {
                CanonicalUser: { 'Fn::GetAtt': ['CloudFrontOriginAccessIdentity', 'S3CanonicalUserId'] }
              },
              Action: 's3:GetObject',
              Resource: { 'Fn::Sub': 'arn:aws:s3:::${hostedName}/*' }
            }
          ]
        }
      }
    },

    AcmCertificate: {
      Type: 'AWS::CertificateManager::Certificate',
      Properties: {
        DomainName: { 'Fn::Sub': '${hostedName}' },
        SubjectAlternativeNames: [
          { 'Fn::Sub': '*.${hostedName}' }
        ],
        ValidationMethod: 'DNS',
        DomainValidationOptions: [
          { DomainName: { 'Fn::Sub': '${hostedName}' }, HostedZoneId: { Ref: 'hostedZoneId' } }
        ]
      }
    },

    CloudFrontResponsePolicy: {
      Type: 'AWS::CloudFront::ResponseHeadersPolicy',
      Properties: {
        ResponseHeadersPolicyConfig: {
          Name: 'AuthBypassResponse',
          CustomHeadersConfig: {
            Items: [{ Header: 'permissions-policy', Override: false, Value: 'publickey-credentials-create=(self), identity-credentials-get=(self)' }]
          },
          RemoveHeadersConfig: {
            Items: [{ Header: 'server' }]
          },
          SecurityHeadersConfig: {
            ContentSecurityPolicy: {
              ContentSecurityPolicy: "default-src * 'unsafe-inline' 'unsafe-eval' 'self' data:",
              Override: false
            },
            ContentTypeOptions: {
              Override: true
            },
            FrameOptions: {
              FrameOption: 'SAMEORIGIN',
              Override: false
            },
            ReferrerPolicy: {
              ReferrerPolicy: 'strict-origin-when-cross-origin',
              Override: false
            },
            StrictTransportSecurity: {
              AccessControlMaxAgeSec: 31536000,
              IncludeSubdomains: true,
              Override: false
            },
            XSSProtection: {
              ModeBlock: true,
              Protection: true,
              Override: false
            }
          }
        }
      }
    },

    CloudFrontDistribution: {
      Type: 'AWS::CloudFront::Distribution',
      Properties: {
        Tags: [
          { Key: 'Name', Value: 'Slack-Auth-Bypass-UI' },
          { Key: 'ServiceUI', Value: { Ref: 'hostedName' } }
        ],
        DistributionConfig: {
          Comment: 'Slack-Auth-Bypass',
          DefaultRootObject: 'index.html',
          Aliases: [
            { 'Fn::Sub': 'tst.${hostedName}' },
            { 'Fn::Sub': '*.${hostedName}' },
            { Ref: 'hostedName' }
          ],
          HttpVersion: 'http2and3',
          PriceClass: 'PriceClass_200',
          Origins: [
            {
              OriginPath: { 'Fn::Sub': '/${deploymentVersion}' },
              DomainName: { 'Fn::Sub': '${hostedName}.s3.amazonaws.com' },
              Id: 'S3',
              S3OriginConfig: {
                OriginAccessIdentity: { 'Fn::Sub': 'origin-access-identity/cloudfront/${CloudFrontOriginAccessIdentity}' }
              }
            },
            {
              DomainName: { 'Fn::Sub': '${hostedName}.s3.amazonaws.com' },
              Id: 'TST-S3',
              S3OriginConfig: {
                OriginAccessIdentity: { 'Fn::Sub': 'origin-access-identity/cloudfront/${CloudFrontOriginAccessIdentity}' }
              }
            }
          ],
          CacheBehaviors: [
            {
              Compress: true,
              AllowedMethods: ['GET', 'HEAD', 'OPTIONS'],
              CachePolicyId: '658327ea-f89d-4fab-a63d-7e88639e58f6',
              OriginRequestPolicyId: '88a5eaf4-2fd4-4709-b370-b4c650ea3fcf',
              ResponseHeadersPolicyId: { Ref: 'CloudFrontResponsePolicy' },
              PathPattern: 'PR-*/*',
              TargetOriginId: 'TST-S3',
              ViewerProtocolPolicy: 'redirect-to-https'
            }
          ],
          Enabled: true,
          ViewerCertificate: {
            AcmCertificateArn: { Ref: 'AcmCertificate' },
            MinimumProtocolVersion: 'TLSv1.2_2021',
            SslSupportMethod: 'sni-only'
          },
          DefaultCacheBehavior: {
            Compress: true,
            AllowedMethods: ['GET', 'HEAD', 'OPTIONS'],
            CachePolicyId: '658327ea-f89d-4fab-a63d-7e88639e58f6',
            OriginRequestPolicyId: '88a5eaf4-2fd4-4709-b370-b4c650ea3fcf',
            ResponseHeadersPolicyId: { Ref: 'CloudFrontResponsePolicy' },
            TargetOriginId: 'S3',
            ViewerProtocolPolicy: 'redirect-to-https'
          },
          CustomErrorResponses: [
            {
              ErrorCode: 403,
              ErrorCachingMinTTL: 300,
              ResponseCode: 200,
              ResponsePagePath: '/app/index.html'
            },
            {
              ErrorCode: 404,
              ErrorCachingMinTTL: 300,
              ResponseCode: 200,
              ResponsePagePath: '/app/index.html'
            }
          ]
        }
      }
    },

    ProdRoute53: {
      Type: 'AWS::Route53::RecordSet',
      Properties: {
        AliasTarget: {
          DNSName: { 'Fn::GetAtt': ['CloudFrontDistribution', 'DomainName'] },
          HostedZoneId: { Ref: 'zoneIdForServiceDomain' }
        },
        HostedZoneId: { Ref: 'hostedZoneId' },
        Name: { 'Fn::Sub': '${hostedName}.' },
        Type: 'A'
      }
    },

    ProdRoute53ipv6: {
      Type: 'AWS::Route53::RecordSet',
      Properties: {
        AliasTarget: {
          DNSName: { 'Fn::GetAtt': ['CloudFrontDistribution', 'DomainName'] },
          HostedZoneId: { Ref: 'zoneIdForServiceDomain' }
        },
        HostedZoneId: { Ref: 'hostedZoneId' },
        Name: { 'Fn::Sub': '${hostedName}.' },
        Type: 'AAAA'
      }
    }
  }
};
