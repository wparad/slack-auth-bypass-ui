#!/usr/bin/env bash

set -e
set -u

echo node version: $(node --version)
echo yarn version: $(yarn --version)

if [ "$CI_COMMIT_REF_NAME" != "master" ]; then
  git config --global user.email "gitlab@rhosys.gitlab.com"
  git config --global user.name "Gitlab Runner"
  git fetch origin
  git merge origin/master --no-edit
fi

yarn install --frozen-lockfile || (tail -20 "yarn-error.log" && exit 1)
yarn lint
yarn build
yarn deploy
