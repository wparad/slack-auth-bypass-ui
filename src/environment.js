function getEnvironment() {
  if (!window || !window.location) {
    return null;
  }
  return 'production';
}

export default getEnvironment();
