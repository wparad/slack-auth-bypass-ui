import { DateTime } from 'luxon';
import store from '../store/store';

/**
 * @description Format date based on locale settings
 */
export default class DateAndTimeFormatter {
  formatDate(timestamp, localeOverride) {
    if (!timestamp) { return null; }
    const locale = localeOverride || store.state.settings?.regionalSettings;
    const dateTime = this.parse(timestamp);
    if (locale === 'ISO8601') {
      return dateTime.toFormat('yyyy-MM-dd');
    }
    return dateTime.setLocale(locale).toLocaleString(DateTime.DATE_SHORT);
  }

  formatDateLong(timestamp) {
    if (!timestamp) {
      return '';
    }
    const dt = this.parse(timestamp);
    if (!dt.isValid) {
      return '';
    }
    const locale = navigator.languages && navigator.languages[0] || navigator.language;
    return dt.setLocale(locale).toLocaleString(DateTime.DATE_MED_WITH_WEEKDAY);
  }

  formatDateLongPretty(timestamp) {
    if (!timestamp) {
      return '';
    }
    const dt = this.parse(timestamp);
    if (!dt.isValid) {
      return '';
    }
    const locale = navigator.languages && navigator.languages[0] || navigator.language;

    if (DateTime.utc().year === dt.year) {
      return dt.setLocale(locale).toLocaleString({ ...DateTime.DATE_MED_WITH_WEEKDAY, year: undefined });
    }

    return dt.setLocale(locale).toLocaleString(DateTime.DATE_MED_WITH_WEEKDAY);
  }

  formatDateMed(timestamp) {
    if (!timestamp) {
      return '';
    }
    const dt = this.parse(timestamp);
    if (!dt.isValid) {
      return '';
    }
    const locale = navigator.languages && navigator.languages[0] || navigator.language;
    return dt.setLocale(locale).toLocaleString(DateTime.DATE_MED);
  }

  /**
   * @description Format date and time based on locale settings
   */
  formatDateTime(timestamp, localeOverride) {
    if (!timestamp) { return null; }
    const locale = localeOverride || store.state.settings?.regionalSettings;
    const dateTime = this.parse(timestamp);
    if (locale === 'ISO8601') {
      return dateTime.toFormat('yyyy-MM-dd HH:mm');
    }
    return `${dateTime.setLocale(locale).toLocaleString(DateTime.DATE_SHORT)} ${dateTime.setLocale(locale).toLocaleString(DateTime.TIME_SIMPLE)}`;
  }

  formatTime(timestamp) {
    return timestamp ? DateTime.fromISO(timestamp).toLocaleString(DateTime.TIME_SIMPLE) : '';
  }

  /**
   * @description Format date and time with timezone based on locale settings
   */
  formatDateTimeZ(timestamp, localeOverride) {
    if (!timestamp) { return null; }
    const locale = localeOverride || store.state.settings?.regionalSettings;
    const dateTime = this.parse(timestamp);
    if (locale === 'ISO8601') {
      return dateTime.toFormat('yyyy-MM-dd HH:mm ZZZZ');
    }
    const datePart = dateTime.setLocale(locale).toLocaleString(DateTime.DATE_SHORT);
    const timePart = dateTime.setLocale(locale).toLocaleString(DateTime.TIME_SIMPLE);
    const timeZonePart = dateTime.setLocale(locale).offsetNameShort;
    return `${datePart} ${timePart} ${timeZonePart}`;
  }

  formatDaysOfWeek(daysObj) {
    const daysNames = ['Mon', 'Tue', 'Wed', 'Thu', 'Fri', 'Sat', 'Sun'];

    const daysArray = Object.keys(daysObj || {})
    .map(dayId => ({ dayNumber: Number(dayId), value: daysObj[dayId] }))
    .sort((a, b) => a.dayNumber - b.dayNumber)
    .map(dayObj => dayObj.value);

    if (!daysArray.length) {
      return 'Not Specified';
    }

    if (daysArray.every(day => day)) {
      return 'Everyday';
    }

    if (daysArray.slice(0, 5).every(day => day) && daysArray.slice(5).every(day => !day)) {
      return 'Weekdays';
    }

    const days = daysNames.filter((_, idx) => daysArray[idx]);
    return days.join(', ');
  }

  parse(timestamp) {
    const dateTimeIso = DateTime.fromISO(timestamp.toString());
    return dateTimeIso.isValid ? dateTimeIso : DateTime.fromRFC2822(timestamp.toString());
  }
}
