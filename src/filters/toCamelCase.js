export default function(value) {
  if (!value) {
    return value;
  }

  const words = value.match(/[A-Z]?[a-z]+/g).map(word => `${word[0].toUpperCase()}${word.substring(1)}`);
  return words.join(' ');
}
