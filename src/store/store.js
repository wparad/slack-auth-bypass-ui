import { DateTime } from 'luxon';
import Vue from 'vue';
import createPersistedState from 'vuex-persistedstate';
import cloneDeep from 'lodash/cloneDeep';
import Vuex from 'vuex';

Vue.use(Vuex);

const plugins = [createPersistedState({ key: 'standup', paths: ['settings', 'profile', 'cache', 'dev', 'permanentCache', 'hiddenNotifications'] })];
const isDevelopmentBuild = process.env.NODE_ENV && process.env.NODE_ENV === 'development';

const emptyState = {
  alert: null,
  globalLoader: false,
  initiatingStartPage: null,
  profile: {},
  hiddenNotifications: {
    StandupReportCopyWarning: null
  },
  cache: {
    redirectOnEnterpriseLogin: true,
    invalidTokenCalls: 0,
    currentTeam: null,
    channels: {},
    channelMembers: {},
    fallbackMembers: {},
    users: {},
    standups: {},
    errorTracking: {
      chunkLoadingError: null,
      networkErrors: 0
    }
  },
  permanentCache: {
    referrerData: null,
    lastUsedTeamId: null
  },
  dev: false
};

function getEmptyState() {
  return cloneDeep(emptyState);
}

export default new Vuex.Store({
  strict: isDevelopmentBuild,
  plugins: plugins,
  state: getEmptyState(),
  mutations: {
    hidePageNotification(state, pageNotificationId) {
      Vue.set(state.hiddenNotifications, pageNotificationId, DateTime.utc().toISO());
    },
    setInitiatingStartPage(state, initiatingStartPage) {
      state.initiatingStartPage = initiatingStartPage;
    },
    updateProfile(state, profile) {
      state.profile = profile;
    },
    updateEnterpriseRedirect(state, value) {
      state.cache.redirectOnEnterpriseLogin = value;
    },
    setAlert(state, alert) {
      state.alert = alert;
    },
    removeAlert(state, options) {
      if (options.force || !state.alert?.persistent) {
        if (options.type) {
          if (state.alert && state.alert.alertType === options.type) {
            state.alert = null;
          }
        } else {
          state.alert = null;
        }
      }
    },
    removeSpecificAlert(state, alert) {
      if (state.alert === alert) {
        state.alert = null;
      }
    },

    setTeam(state, team) {
      state.cache.currentTeam = team;
      state.permanentCache.lastUsedTeamId = team?.teamId;
    },

    setChannels(state, channels) {
      state.cache.channels = channels || {};
      Object.keys(state.cache.channels).map(channelId => {
        if (!state.cache.channelMembers[channelId]) {
          // so changes are reactive
          Vue.set(state.cache.channelMembers, channelId, null);
        }
      });
    },
    setChannelMembers(state, { channelId, members }) {
      state.cache.channelMembers[channelId] = members;
    },
    setMember(state, { memberId, member }) {
      if (!state.cache.fallbackMembers) {
        state.cache.fallbackMembers = {};
      }

      if (!member) {
        delete state.cache.fallbackMembers[memberId];
        return;
      }
      state.cache.fallbackMembers[memberId] = member;
    },
    removeLogin(state, clearEverything) {
      const isDev = state.dev;
      const newEmptyState = getEmptyState();
      const permanentCache = state.permanentCache;
      Object.keys(newEmptyState).forEach(key => {
        state[key] = newEmptyState[key];
      });
      state.permanentCache = permanentCache;
      if (clearEverything) {
        state.permanentCache.lastUsedTeamId = null;
      }
      state.dev = isDev;
    },
    setStandup(state, standup) {
      state.cache.standups[standup.standupId] = standup;
    },
    setStandups(state, standups) {
      state.cache.standups = standups || {};
    },
    setAppVersionAsDev(state, dev) {
      state.dev = dev;
    },

    trackHttpSuccess(state) {
      state.cache.errorTracking.networkErrors = 0;
    },

    trackChunkLoadingError(state, version) {
      state.cache.errorTracking.chunkLoadingError = version;
    },

    trackHttpError(state, errorInfo) {
      if (errorInfo.error.message === 'Network Error' || errorInfo.error.code === 'NetworkError') {
        state.cache.errorTracking.networkErrors++;
      }
    },

    setInvalidTokenCalls(state, value) {
      state.cache.invalidTokenCalls = value || 0;
    },
    setReferrerData(state, referrerData) {
      state.permanentCache.referrerData = Object.values(referrerData || {}).some(v => v) ? referrerData : state.permanentCache.referrerData;
    },

    setGlobalLoader(state, value) {
      state.globalLoader = value;
    }
  },
  getters: {
    memberMap(state) {
      const memberMap = cloneDeep(state.cache.fallbackMembers || {});
      Object.values(state.cache.channelMembers || {}).map(channelMemberMap => {
        Object.assign(memberMap, channelMemberMap);
      });
      return memberMap;
    },
    isDev(state) {
      return state.dev;
    }
  }
});
