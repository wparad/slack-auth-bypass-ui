import multiguard from 'vue-router-multiguard';

const BypassStart = () => import(/* webpackChunkName: "bypass" */ './components/bypass');

export default function routes(authManager) {
  const requireUnlockedTeamAccount = multiguard([authManager.loggedInGuard()]);
  return [
    {
      path: '/bypass-start',
      name: 'Bypass-start',
      component: BypassStart,
      beforeEnter: requireUnlockedTeamAccount
    },
    {
      path: '*',
      redirect: '/bypass-start'
    }
  ];
}
