import 'url-polyfill';
import jwtManager from './jwtManager';
import { DateTime } from 'luxon';
import { LoginClient } from '@authress/login';
import store from '../store/store';

export default class AuthManager {
  constructor(alertHandler) {
    this.alertHandler = alertHandler;
    this.loginClient = new LoginClient({ authenticationServiceUrl: 'https://security.standup-and-prosper.com', applicationId: 'app_xwJtbeH9uy718gaDFe7AZJ' });

    this.loginHooks = [];
    this.initialized = false;
  }

  addLoginHook(hook) {
    this.loginHooks.push(hook);
  }

  async trackUnauthorizedRequest() {
    if (store.state.cache.invalidTokenCalls >= 1) {
      console.log({ title: 'total allowed unauthorized requests exceeded, resetting the token.',
        count: store.state.cache.invalidTokenCalls, source: 'authManager.trackUnauthorizedRequest' });
      store.commit('setInvalidTokenCalls', 0);
      await this.logout();
      setTimeout(() => window.location.reload(), 200);
      return;
    }

    console.log({ title: 'Tracking unauthorized request', count: store.state.cache.invalidTokenCalls, source: 'authManager.trackUnauthorizedRequest' });
    store.commit('setInvalidTokenCalls', store.state.cache.invalidTokenCalls + 1);
  }

  loggedInGuard() {
    const stripQueryFromWindow = () => {
      const replacement = new URL(window.location.href);
      replacement.search = '';
      window.history.replaceState({}, document.title, replacement.toString());
    };
    return async (to, from, next) => {
      console.debug({ title: 'LoggedInGuard' });

      // require login is a guard so fundamentally we are not at the location we want to be after the redirect.  So let's save instead the new destination, and navigate there instead of here.
      const hashLocation = window.location.href.indexOf('#');
      const origin = hashLocation !== -1 ? window.location.href.substring(0, hashLocation) : window.location.href;
      const options = {
        redirectUri: `${origin}#${to.fullPath}`
      };

      let result;
      try {
        result = await this.ensureLoggedIn(options);
        if (result?.redirectUri) {
          next(false);
          window.location.replace(result.redirectUri);
          return;
        }
      } catch (error) {
        // If the provider decides that there was a problem, assume the user knows what is going on, it is possible that they don't though.
        if (error.errorCode === 'access_denied') {
          console.warn({ title: 'User denied access to login', to, from, next, options, location: window.location, error });
          next({ name: 'Unauthorized', replace: true });
          return;
        }

        console.warn({ title: 'Failed to log the user in', to, from, next, options, location: window.location, error });
        this.alertHandler.createAlert('Login unsuccessful', 'Slack failed to respond with a valid authentication token.<br>This can occur if Slack is currently having a status incident. <br><br>Automatically retrying...<br><br><small>If you continue to experience issues, please contact <a href="mailto:security@rhosys.ch">security@rhosys.ch</a>.</small>', 'danger', 4000, true);
        next(false);
        setTimeout(async () => {
          try {
            this.alertHandler.removeAlert('danger', true);
            await this.logout();
            console.warn({ title: 'Retrying user login after a force logout', to, from, next, options, location: window.location, error });
            await this.ensureLoggedIn(options);
          } catch (innerError) {
            console.error({ title: 'Failed to log the user in on retry', to, from, next, options, location: window.location, error, innerError });
            this.alertHandler.createAlert('Login unsuccessful', 'Slack failed to respond with a valid authentication token.<br>This can occur if Slack is currently having a status incident. Please try again by using the Log in button in the navigation bar.<br><br>If you continue to experience issues, please contact <a href="mailto:security@rhosys.ch">security@rhosys.ch</a>.', 'danger', 10000, true);
          }
        }, 4000);
        return;
      }

      let currentToken;
      try {
        currentToken = await this.getToken({ requireValidSession: true });
      } catch (error) {
        if (error.code === 'TokenTimeout') {
          console.log({ title: 'Timeout waiting for token for loggedInGuard, redirecting user to the Unauthorized screen, this can happen if the tab is still running while the user is redirected to Slack for login. In this situation we should just ignore it. If the LocalStorage has a AuthenticationRequestNonce set, why did it not get exchanged? Investigate. Perhaps Authress did not exchange the token correctly.',
            to, from, next, options, location: window.location, token: currentToken, result, store: store.state, localStorage: window.localStorage, cookies: document.cookie });
        } else {
          console.log({ title: `No token found, it is probably a timeout but we should validate this. If it is always a timeout then remove the warning. - ${error.code} - ${error.message}`,
            to, from, next, options, location: window.location, token: currentToken, result, store: store.state, localStorage: window.localStorage, cookies: document.cookie });
        }
        next({ name: 'Unauthorized', replace: true });
        return;
      }

      let identity;
      try {
        identity = jwtManager.decode(currentToken);
      } catch (error) {
        console.error({ title: 'Failed to decode user id token', to, from, next, options, location: window.location, error, token: currentToken, result });
        this.alertHandler.createAlert('Login unsuccessful', 'The login response from Slack during log in was invalid.<br><br>This can occur if Slack is currently having a status incident or the permission access was disallowed during login. Please try again by using the Log in button in the navigation bar.<br><br>If you continue to experience issues, please contact <a href="mailto:security@rhosys.ch">security@rhosys.ch</a>.', 'danger', 10000, true);
        next({ name: 'Unauthorized', replace: true });
        return;
      }

      if (!identity) {
        next(false);
        await this.logout();
        return;
      }

      try {
        const userData = await this.loginClient.getUserIdentity();
        const profile = {
          expiresAt: DateTime.local().plus({ seconds: identity.exp - identity.iat }).toISO(),
          userId: userData.data?.user?.id || userData.data?.sub,
          picture: userData.data?.user?.image_32 || userData.data?.user?.profile?.image_32 || userData.data?.['https://slack.com/user_image_32'] || userData.picture,
          displayName: userData.data?.user?.name || userData.data?.name,

          teamId: userData.sub.split('|')[1] || userData.data?.['https://slack.com/team_id'],
          teamName: userData.data?.team?.name || userData.data?.team?.domain || userData.context?.team?.name || userData.context?.enterprise?.name || userData.data?.['https://slack.com/team_name'],
          teamPicture: userData.data?.['https://slack.com/team_image_34'],
          isEnterpriseInstallation: userData.context?.enterprise?.id,
          tokenType: identity.azp === 'con_3wpSMkFgaPGkx56vjKv4jt' ? 'IDENTITY' : 'INSTALLER'
        };
        if (profile.picture === 'https://avatars.slack-edge.com/' && profile.teamPicture) {
          profile.picture = profile.teamPicture;
        }
        store.commit('updateProfile', profile);

        if (!this.initialized) {
          await Promise.all(this.loginHooks.map(hook => hook()));
          this.initialized = true;
        }
        stripQueryFromWindow();
        next();
        return;
      } catch (error) {
        console.error({ title: 'Failed to set profile from user identity.', to, from, next, options, location: window.location, error });
        if (from.name === 'Unauthorized') {
          next(false);
          await this.logout();
          return;
        }
        this.alertHandler.createAlert('Login unsuccessful', 'The login response from Slack during log in was invalid.<br><br>This can occur if Slack is currently having a status incident or the permission access was disallowed during login. Please try again by using the Log in button in the navigation bar.<br><br>If you continue to experience issues, please contact <a href="mailto:security@rhosys.ch">security@rhosys.ch</a>.', 'danger', 10000, true);
        next({ name: 'Unauthorized', replace: true });
      }
    };
  }

  async ensureLoggedIn(options) {
    try {
      const isUserLoggedIn = await this.loginClient.userSessionExists();
      if (isUserLoggedIn && !options.force) {
        return {};
      }

      const teamId = options.teamId || !options.disableCachedTeamId && store.state.profile.teamId || !options.disableCachedTeamId && store.state.permanentCache.lastUsedTeamId || '';
      const userData = await this.loginClient.getUserIdentity();
      console.log({ title: 'Redirecting to authentication because the user is not logged in', isUserLoggedIn, options, teamId, cookies: document.cookie,
        currentUserData: userData, AuthenticationCredentialsStorage: localStorage.AuthenticationCredentialsStorage });

      // We are potentially going to lose the list of logs here so force a flush before leaving
      await this.loginClient.authenticate({
        force: options?.force, redirectUrl: options.redirectUri,
        connectionId: !options?.install ? 'con_3wpSMkFgaPGkx56vjKv4jt' : 'con_4PyvSErXdpMrT18NuHFRrU',
        connectionProperties: {
          team: teamId?.replace(/^slack[|]/i, ''),
          scope: !options?.install ? undefined : 'app_mentions:read,channels:read,chat:write,chat:write.public,chat:write.customize,commands,groups:read,im:history,im:read,im:write,mpim:read,users:read',
          user_scope: !options?.install ? 'email openid profile' : 'email openid profile'
        }
      });

      return {};
    } catch (error) {
      if (error.message === 'Network Error' || error.code === 'NetworkError' || error.message === 'Load failed' || error.message === 'Failed to fetch') {
        console.warn({ title: 'Failed to ensureLoggedIn due to a network error', options, error });
      } else {
        console.error({ title: 'Failed to ensureLoggedIn', options, error });
      }
      throw error;
    }
  }

  async logout() {
    console.log({ title: 'User logging out, clearing session' });
    store.commit('removeLogin', true);
    store.commit('setGlobalLoader', true);
    const reloadLocation = new URL(window.location);
    reloadLocation.pathname = '/';
    reloadLocation.hash = '';
    await this.loginClient.logout(reloadLocation.toString());
    store.commit('setGlobalLoader', false);
  }

  async getToken(options = { timeoutInMillis: 10000, requireValidSession: false }) {
    try {
      const token = await this.loginClient.ensureToken({ timeoutInMillis: options?.timeoutInMillis || 10000 });
      if (!token) {
        if (options?.requireValidSession) {
          throw Error.create('NoTokenFoundFromAuthress');
        }

        console.log({ title: 'No token found from Authress, review the session and cookie data and look for why this might have happened, we probably should just redirect the user to the unauthorized page.',
          options, location: window.location, store: store.state, localStorage: window.localStorage, cookies: document.cookie });
      }
      return token;
    } catch (error) {
      console.log({ title: 'Failed to wait for token', error, options });
      if (options?.requireValidSession) {
        throw error;
      }
      return null;
    }
  }

  // TODO: test this actually works with logout correctly and we don't end up in not actually forcing the installation
  async forceInstallation() {
    const teamId = store.state.profile.teamId;
    console.log({ title: 'Forcing log out and retrying log in with installation' });
    store.commit('removeLogin');
    store.commit('setGlobalLoader', true);

    for (let iteration = 0; iteration < 20; iteration++) {
      try {
        // The team: 1 is a magic, not sure if it even works correctly all the time though.
        await this.ensureLoggedIn({ force: true, redirectUri: window.location.href, teamId, install: true });
        return;
      } catch (error) {
        console.warn({ title: 'Failed to force user login with Installation', error, iteration });
        await new Promise(resolve => setTimeout(resolve, 20 * 2 ** iteration));
      }
    }
  }
}
