import { decode } from './base64url.js';

class JwtManager {
  decode(token) {
    try {
      return token && JSON.parse(decode(token.split('.')[1]));
    } catch (error) {
      return null;
    }
  }

  decodeFull(token) {
    try {
      return token && {
        header: JSON.parse(decode(token.split('.')[0])),
        payload: JSON.parse(decode(token.split('.')[1]))
      };
    } catch (error) {
      return null;
    }
  }
}

export default new JwtManager();
