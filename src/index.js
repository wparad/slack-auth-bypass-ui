/* eslint-disable vue/multi-word-component-names */
import 'error-object-polyfill';

import '@fontsource/courier-prime/400.css';
import '@fontsource/courier-prime/700.css';
import '@fontsource/roboto/300.css';
import '@fontsource/roboto/400.css';
import '@fontsource/roboto/500.css';
import '@fontsource/roboto/700.css';
import '@fontsource/lato/300.css';
import '@fontsource/lato/400.css';
import '@fontsource/lato/700.css';

import Vue from 'vue';
import VueRouter from 'vue-router';
import VeeValidate from 'vee-validate';
import VueScrollTo from 'vue-scrollto';
import VueClipboard from 'vue-clipboard2';
import BootstrapVue from 'bootstrap-vue';

import alertHandler from './errorHandling/alertHandler';

/* eslint-ignore-next-line ode/no-missing-require */
require('./components/shared/responsiveToolkit');
import routes from './routes';
import store from './store/store';

const setToTrue = new URL(window.location).searchParams.get('dev') === 'true';
const setValue = setToTrue || new URL(window.location).searchParams.get('dev') === 'false';
if (setValue) {
  store.commit('setAppVersionAsDev', setToTrue);
}

store.commit('setInitiatingStartPage', window.location.href);

import OnCreatedComponentMixinProvider from './components/onCreatedComponentMixinProvider';
import AuthManager from './auth/authManager';

Vue.use(VueRouter);
Vue.use(VeeValidate, { fieldsBagName: 'veeFields' });
Vue.use(VueScrollTo);
// https://www.npmjs.com/package/vue-clipboard2#it-doesnt-work-with-bootstrap-modals
VueClipboard.config.autoSetContainer = true;
Vue.use(VueClipboard);

Vue.use(BootstrapVue);

import 'bootstrap-vue/dist/bootstrap-vue.css';
import tmrButton from './components/shared/activityButton.vue';
import Loader from './components/shared/loader';
Vue.component('t-button', tmrButton);
Vue.component('loader', Loader);
import staticLoader from './components/shared/staticLoader.vue';
Vue.component('static-loader', staticLoader);

import VCalendar from 'v-calendar';
Vue.use(VCalendar, {});

// https://github.com/FortAwesome/vue-fontawesome
import { FontAwesomeIcon } from '@fortawesome/vue-fontawesome';
Vue.component('fa', FontAwesomeIcon);
import { library } from '@fortawesome/fontawesome-svg-core';
library.add(require('@fortawesome/free-solid-svg-icons/faSpinner').faSpinner);
library.add(require('@fortawesome/free-solid-svg-icons/faCheck').faCheck);
library.add(require('@fortawesome/free-solid-svg-icons/faExternalLinkSquareAlt').faExternalLinkSquareAlt);

const App = () => import(/* webpackChunkName: "main" */ './app.vue');

function init() {
  const appInitializer = () => {};
  Vue.mixin(new OnCreatedComponentMixinProvider().getMixin({}, appInitializer));

  const authManager = new AuthManager(alertHandler);

  const router = new VueRouter({
    routes: routes(authManager),
    store,
    linkActiveClass: 'active'
  });

  authManager.addLoginHook(async () => {
    console.log('*****', store.state.profile.tokenType);
    if (store.state.profile.tokenType === 'IDENTITY') {
      await authManager.forceInstallation();
    }
  });

  new Vue({
    router,
    store,

    provide: {
      alertHandler,
      sessionStore: store,
      authManager
    },

    created() {
      router.afterEach(to => {
        document.title = this.getDocumentTitle(to);
        window.scrollTo(0, 0);
        alertHandler.removeAlert('danger', false);
        alertHandler.removeAlert('warning', false);
      });
      document.title = this.getDocumentTitle(this.$route);
    },
    watch: {
      // watch any changes to the route and ensure permissions
      $route(newRoute) {
        const setToTrueInternal = newRoute.query.dev === 'true' || new URL(window.location).searchParams.get('dev') === 'true';
        const setValueInternal = setToTrueInternal || newRoute.query.dev === 'false' || new URL(window.location).searchParams.get('dev') === 'false';
        if (setValueInternal) {
          store.commit('setAppVersionAsDev', setToTrueInternal);
        }
      }
    },

    methods: {
      getDocumentTitle(route = null) {
        if (!route || !route.name) {
          return 'AuthBypass';
        }

        return `AuthBypass - ${route.name}`;
      }
    },
    render: h => h(App)
  }).$mount('#app');
}

init();
