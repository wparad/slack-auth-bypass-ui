/* eslint-disable no-console */

/**
 * Module dependencies
 */
const aws = require('aws-sdk');
const commander = require('commander');
const AwsArchitect = require('aws-architect');
const path = require('path');
const { DateTime } = require('luxon');

const version = `0.0.${process.env.CI_PIPELINE_ID}`;
commander.version(version);
const REGION = 'us-east-1';
aws.config.update({ region: REGION });

const packageMetadataFile = path.join(__dirname, 'package.json');
const packageMetadata = require(packageMetadataFile);

const parameters = {
  hostedName: 'slack-auth-bypass.rhosys.ch',
  hostedZoneId: 'Z23O6KKMS7U8YR',
  deploymentVersion: `v${DateTime.utc().year}`
};

const apiOptions = {
  deploymentBucket: `rhosys-microservice-deployment-artifacts-${REGION}`
};

const contentOptions = {
  bucket: `${parameters.hostedName}`,
  contentDirectory: path.join(__dirname, 'build')
};

async function setupAWS() {
  if (!process.env.CI_JOB_JWT_V2) { return; }
  aws.config.credentials = new aws.WebIdentityCredentials({
    WebIdentityToken: process.env.CI_JOB_JWT_V2,
    RoleArn: `arn:aws:iam::${process.env.AWS_ACCOUNT_ID}:role/GitlabRunnerAssumedRole`,
    RoleSessionName: `GitLabRunner-${process.env.CI_PROJECT_PATH_SLUG}-${process.env.CI_PIPELINE_ID}`,
    DurationSeconds: 3600
  });

  const stsResult = await new aws.STS().getCallerIdentity().promise();
  console.log('Configured AWS Credentials', stsResult);
}

commander
.command('deploy')
.description('Deploying website to AWS.')
.action(async () => {
  process.env.CI_COMMIT_REF_SLUG = 'master';
  process.env.CI_PIPELINE_ID = Math.round(Date.now() / 1000 / 60);

  if (!process.env.CI_COMMIT_REF_SLUG) {
    console.log('Deployment should not be done locally.');
    return;
  }
  await setupAWS();

  let deploymentVersion = parameters.deploymentVersion;
  let deploymentLocation = `https://${contentOptions.bucket}/`;

  const stackTemplate = require('./cloudFormationWebsiteTemplate');
  const awsArchitect = new AwsArchitect(packageMetadata, apiOptions, contentOptions);

  try {
    await awsArchitect.validateTemplate(stackTemplate);
    const isMasterBranch = process.env.CI_COMMIT_REF_SLUG === 'master';

    if (isMasterBranch) {
      const stackConfiguration = {
        changeSetName: `${process.env.CI_COMMIT_REF_SLUG}-${process.env.CI_PIPELINE_ID || '1'}`,
        stackName: packageMetadata.name,
        tags: { Service: packageMetadata.name }
      };
      await awsArchitect.deployTemplate(stackTemplate, stackConfiguration, parameters);
    } else {
      deploymentVersion = `PR-${process.env.CI_COMMIT_REF_SLUG}`;
      deploymentLocation = `https://tst.${contentOptions.bucket}/${deploymentVersion}/index.html`;
    }

    const publishConfig = {
      configureBucket: false,
      cacheControlRegexMap: [
        { explicit: 'index.html', value: 'no-store' },
        { regex: new RegExp(/static\/images/), value: 'no-store' },
        { regex: new RegExp(/static\/slack/), value: 'no-store' },
        { regex: new RegExp(/static/), value: 'no-store' },
        { explicit: 'app/index.html', value: 'no-store' },
        { value: 'no-store' }
      ]
    };
    const result = await awsArchitect.publishWebsite(deploymentVersion, publishConfig);

    console.log(JSON.stringify(result, null, 2));
    console.log(`Deployed to ${deploymentLocation}`);
  } catch (error) {
    console.log(`Deployment failed: ${JSON.stringify(error, null, 2)}`);
    process.exit(1);
  }
});

commander.on('*', () => {
  if (commander.args.join(' ') === 'tests/**/*.js') {
    return;
  }
  console.log(`Unknown Command: ${commander.args.join(' ')}`);
  commander.help();
  process.exit(0);
});
commander.parse(process.argv[2] ? process.argv : process.argv.concat(['build']));
