/* eslint-disable quote-props */
module.exports = {
  'parser': 'vue-eslint-parser',
  'parserOptions': {
    'parser': '@babel/eslint-parser',
    'sourceType': 'module',
    'ecmaVersion': 2021
  },
  'env': {
    'es6': true,
    'node': true,
    'browser': true
  },
  'plugins': [
    'vue',
    'mocha',
    'promise'
  ],
  'extends': [
    'plugin:vue/essential',
    'cimpress-atsquad'
  ],
  'rules': {
    'arrow-parens': ['error', 'as-needed'],
    'curly': ['error', 'all'],
    'indent': ['error', 2, { 'SwitchCase': 1, 'MemberExpression': 0 }],
    'max-len': ['error', { 'code': 200, 'tabWidth': 2, 'ignoreStrings': true, 'ignoreUrls': true, 'ignoreTemplateLiterals': true, 'ignoreComments': true, 'ignoreTrailingComments': true }],
    'node/no-unsupported-features': ['off'],
    'no-console': 'off',
    'prefer-const': ['error'],
    'node/no-unsupported-features/es-syntax': ['off'],
    'node/no-unpublished-import': ['off'],
    'node/no-missing-import': ['error', {
      'tryExtensions': ['.js', '.json', '.vue', '.css']
    }],
    'vue/singleline-html-element-content-newline': 'off',
    'vue/multiline-html-element-content-newline': 'off',
    'vue/order-in-components': 'off',
    'vue/max-attributes-per-line': 'off',
    'vue/attributes-order': 'off',
    'vue/html-indent': 'off',
    'vue/html-closing-bracket-newline': ['error', {
      'singleline': 'never',
      'multiline': 'never'
    }],
    'vue/attribute-hyphenation': 'off'
  },
  'settings': {
    'import/resolver': {
      'alias': {
        'map': [
          ['@/*', './src/*']
        ],
        'extensions': ['.ts', '.js', '.jsx', '.vue']
      }
    }
  }
};
