// `./node_modules/@vue/cli-service/bin/vue-cli-service.js inspect > webpack.config.js` to see webpack output
// const path = require('path');
const webpack = require('webpack');
const externals = require('webpack-node-externals');
const path = require('path');

const isProductionBuild = process.env.NODE_ENV !== 'development';
const isPullRequest = process.env.CI_COMMIT_REF_SLUG && process.env.CI_COMMIT_REF_SLUG !== 'master';
// eslint-disable-next-line no-console
console.log('Production Build: ', isProductionBuild);
module.exports = {
  // Don't turn this on unless we skip having the index.html in the cache or serving via service worker. It seems that they can get out of date and have a mismatch then.
  // integrity: true,
  lintOnSave: isProductionBuild ? 'error' : true,
  outputDir: 'build/app',
  publicPath: isProductionBuild ? (isPullRequest ? `/PR-${process.env.CI_COMMIT_REF_SLUG}/app/` : '/app/') : '/',
  devServer: {
    host: 'localhost'
  },

  configureWebpack(config) {
    if (!isProductionBuild) {
      config.devtool = 'inline-source-map';
    }
    if (process.env.NODE_ENV === 'test') {
      // exclude NPM deps from test bundle
      module.exports.externals = [externals()];
    }

    config.plugins.push(new webpack.DefinePlugin({
      VERSION_INFO: JSON.stringify({
        releaseDate: new Date().toISOString(),
        buildNumber: process.env.CI_PIPELINE_ID,
        buildRef: process.env.CI_COMMIT_REF_NAME,
        buildCommit: process.env.CI_COMMIT_SHORT_SHA
      }),
      DEPLOYMENT_INFO: JSON.stringify({
        FDQN: process.env.HOSTED_NAME
      })
    }));

    if (process.env.NODE_ENV !== 'test') {
      config.optimization = {
        splitChunks: {
          name: true,
          chunks: 'all',
          automaticNameDelimiter: '.'
        }
      };
    }

    config.resolve.alias = {
      '@': require('path').resolve(__dirname, './src')
    };

    config.resolve.extensions = ['.js', '.jsx', '.vue', '.json'];

    // Attempt to fix lit-element duplication issue
    // const { DuplicatesPlugin } = require('inspectpack/plugin');
    // config.plugins.push(new DuplicatesPlugin({ emitErrors: false, verbose: true }));
  },

  chainWebpack(config) {
    config.module
    .rule('vue')
    .use('vue-loader')
    .loader('vue-loader')
    .tap(options => {
      // vue components that are on multiple lines should be separated on the output by a space, making this the default behavior instead of requiring css between everything
      options.compilerOptions.preserveWhitespace = true;
      return options;
    });
  },

  pluginOptions: {
    'style-resources-loader': {
      preProcessor: 'scss',
      patterns: [
        // path.resolve(__dirname, 'src/styles/_shared.scss')
      ]
    }
  },

  css: {
    loaderOptions: {
      scss: {
        additionalData(source, loaderContext) {
          const { resourcePath, rootContext } = loaderContext;
          const relativePath = path.relative(rootContext, resourcePath);
          if (relativePath.endsWith('_vars.scss')) {
            return source;
          }

          return `@import "src/styles/_vars.scss";\n\n${source}`;
        }
      }
    }
  },

  transpileDependencies: ['openapi-explorer']

};
